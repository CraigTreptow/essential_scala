"foo".take(1) -> "foo" take 1
1 + 2 + 3 -> 1.+(2).+(3)
// 1 + 2 + 3 vs 6
// They both evaluate to the same value
