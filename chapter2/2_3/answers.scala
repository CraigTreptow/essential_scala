// What are the values and types?
//
// 42 -> value: 42, type: Int
// true -> value: true, type: Bool
// 123L -> value: 123, type: Long
// 42.0 -> value: 42, type: Double
//
// What is the difference between theese literals?  What are the types?
// 'a' -> A single character of type Char
// "a" -> A single character of type String
//
//What are the values and types of these expressions?
// "Hello world!" -> A literal String, type: String
// println ( "Hello world!" ) -> outputs the same characters, type: Unit
//
