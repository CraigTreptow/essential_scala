object calc {
  def square(x: Double): Double = {
    x * x
  }

  def cube(x: Double): Double = {
    x * x * x
  }
}
