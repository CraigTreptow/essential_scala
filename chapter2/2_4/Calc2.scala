// generalized for Int or Double
// had to look, but pattern matching!!

object calc {
  def square(x: Double): Double = x * x
  def cube(x: Double): Double = x * square(x)

  def square(x: Int): Int = x * x
  def cube(x: Int): Int = x * square(x)
}
