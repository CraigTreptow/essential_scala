object person {
  val firstName = "Craig"
  val lastName = "Treptow"
}

object alien {
  def greet(p: person.type): String = {
    "Greetings, " + p.firstName + " " + p.lastName
  }
}

// the type of greet is specific to the person object, so it cannot
// be used with anything else
