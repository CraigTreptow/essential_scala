object Calc extends App {
  def square(x: Double): Double = { x * x }

  assert ( square ( 2.0 ) == 4.0 )
  assert ( square ( 3.0 ) == 9.0 )
  assert ( square (- 2.0 ) == 4.0 )
}

