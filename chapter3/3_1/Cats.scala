class Cat(given_name: String, given_color: String, given_food: String) {
  val name  = given_name
  val color = given_color
  val food  = given_food
}

object ChipShop {
  def willServe(cat: Cat): Boolean ={
    cat.food.toLowerCase == "chips"
  }
}

// val c = new Cat(given_name = "c", given_color = "blue", given_food = "Chips")
// ChipShop.willServe(c)
// res0: Boolean: true
