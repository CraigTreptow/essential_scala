class Adder (amount: Int) {
  def add(in: Int) = in + amount
}

class Counter(val count: Int) {
  def inc(n: Int = 1) = new Counter(this.count + n)
  def dec(n: Int = 1) = new Counter(this.count - n)
  def adjust(adder: Adder) = new Counter(adder.add(this.count))
}

object Counting extends App {
  val x = new Counter(10).inc().dec().inc().inc().count
  val y = new Counter(10).inc().dec(5).inc().inc().count
  println(x)
  println(y)
}
