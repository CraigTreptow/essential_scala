class Adder (amount: Int) {
  def apply (in: Int): Int = in + amount
}

object AddThings extends App{
  val add3 = new Adder(3)

  // convention of using the method 'apply'
  // allows the shorthand usage
  // This is "funcation application syntax"
  println(add3.apply(2))
  println(add3(4))
}
