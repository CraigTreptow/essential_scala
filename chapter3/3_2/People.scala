class Person( val first_name: String, val last_name: String)

object Person {
  def apply(name: String): Person = {
    val s_name = name.split(" ")
    new Person(s_name(0), s_name(1))
  }
}

object People extends App {
  val f_name = Person("Paul Treptow").first_name
  val l_name = Person("Paul Treptow").last_name
  println(f_name)
  println(l_name)
}
