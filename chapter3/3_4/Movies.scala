case class Director(firstName: String, lastName: String, yearOfBirth: Int) {
  def name: String = s"$firstName $lastName"
}

object Director {
  def older(director_a: Director, director_b: Director): Director = {
    if (director_a.yearOfBirth < director_b.yearOfBirth) director_a else director_b
  }
}

case class Film(name: String, yearOfRelease: Int, imdbRating: Double, director: Director) {
  def directorsAge = yearOfRelease - director.yearOfBirth

  def isDirectedBy(director: Director): Boolean = {
    this.director == director
  }
}

object Film {
  def highestRating(film_a: Film, film_b: Film): Double = {
    if (film_a.imdbRating > film_b.imdbRating) film_a.imdbRating else film_b.imdbRating
  }

  def oldestDirectorAtTheTime(film_a: Film, film_b: Film): Director = {
    if (film_a.directorsAge > film_b.directorsAge) film_a.director else film_b.director
  }

  def newer(film_a: Film, film_b: Film): Film = {
    if (film_a.yearOfRelease < film _b.yearOfRelease) film_a else film_b
  }
}

object Movies extends App {
  val eastwood = new Director( "Clint" , "Eastwood" , 1930 )
  val mcTiernan = new Director( "John" , "McTiernan" , 1951 )
  val nolan = new Director( "Christopher" , "Nolan" , 1970 )
  val someBody = new Director( "Just" , "Some Body" , 1990 )
  val memento = new Film( "Memento" , 2000 , 8.5 , nolan)
  val darkKnight = new Film( "Dark Knight" , 2008 , 9.0 , nolan)
  val inception = new Film( "Inception" , 2010 , 8.8 , nolan)
  val highPlainsDrifter = new Film( "High Plains Drifter" , 1973 , 7.7 , eastwood)
  val outlawJoseyWales = new Film( "The Outlaw Josey Wales" , 1976 , 7.9 , eastwood)
  val unforgiven = new Film( "Unforgiven" , 1992 , 8.3 , eastwood)
  val granTorino = new Film( "Gran Torino" , 2008 , 8.2 , eastwood)
  val invictus = new Film( "Invictus" , 2009 , 7.4 , eastwood)
  val predator = new Film( "Predator" , 1987 , 7.9 , mcTiernan)
  val dieHard = new Film( "Die Hard" , 1988 , 8.3 , mcTiernan)
  val huntForRedOctober = new Film( "The Hunt for Red October" , 1990 , 7.6 , mcTiernan)
  val thomasCrownAffair = new Film( "The Thomas Crown Affair" , 1999 , 6.8 , mcTiernan)

  println(granTorino.isDirectedBy(eastwood))

  highPlainsDrifter.copy(name = "L'homme des hautes plaines" )
  thomasCrownAffair.copy(yearOfRelease = 1968 , director = new Director ( "Norman" , "Jewison" , 1926 ))
  inception.copy().copy().copy()

  println(highPlainsDrifter)

  val oldest_director = Director.older(eastwood, nolan)
  println(oldest_director.lastName)

  val highest_rating = Film.highestRating(unforgiven, granTorino)
  println(highest_rating)

  val oldest_director_at_the_time = Film.oldestDirectorAtTheTime(unforgiven, granTorino)
  println(oldest_director_at_the_time.lastName)
}

