class Person( val first_name: String, val last_name: String)

object Person {
  def apply(name: String): Person = {
    val s_name = name.split(" ")
    new Person(s_name(0), s_name(1))
  }
}

// case classes get stuff automatically created by Scala.  Such as:
// - both the class and companion object
// - a field for each contstructor argument
// - a default toString method
// - equals and hashCode methods that operate on the field values of the object
// - a copy method that returns a new object of that class
//
case class Person2 (firstName: String, lastName: String) {
  def name = firstName + " " + lastName
}

class Citizen1 {
  def firstName = "John1"
  def lastName = "Doe1"
  def name = firstName + " " + lastName
}

// has a more meaningful toString (see below)
case object Citizen2 {
  def firstName = "John2"
  def lastName = "Doe2"
  def name = firstName + " " + lastName
}

object People extends App {
  val f_name = Person("Paul Treptow").first_name
  val l_name = Person("Paul Treptow").last_name
  println(f_name)
  println(l_name)

  val dave = new Person2("Dave", "Donner")
  val dave2 = dave.copy(firstName = "Dave2")
  println(dave.name)
  println(dave.firstName)
  println(dave.toString)
  println(dave2.name)

  println("Two instances are equal when comparing attribute values")
  val equal_or_not_1 = new Person2("Noel", "Welsh").equals(new Person2("Noel", "Welsh"))
  val equal_or_not_2 = new Person2("Noel", "Welsh") == new Person2("Noel", "Welsh")
  println(equal_or_not_1)
  println(equal_or_not_2)

  println("The second is a case object with a better toString result")
  val cit1 = new Citizen1
  println(cit1.toString)
  println(Citizen2.toString)
}
