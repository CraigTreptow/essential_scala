case class Cat(name: String, color: String, food: String)

object ChipShop {
  def willServe(cat: Cat): Boolean = {
    cat match {
      case Cat(_, _, "chips") => true
      case Cat(_,_,_)         => false
    }
  }
}

object Cats extends App {
  val c  = new Cat("c", "blue", "Chips")
  val cc = new Cat("c", "blue", "chips")

  println(ChipShop.willServe(c))
  println(ChipShop.willServe(cc))
}
