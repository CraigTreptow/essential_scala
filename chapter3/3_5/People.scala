case class Person(first_name: String, last_name: String)

object Stormtrooper {
  def inspect (person: Person): String = {
    person match {
      case Person("Luke", "Skywalker") => "Stop, rebel scum!"
      case Person("Han", "Solo")       => "Stop, rebel scum!"
      case Person(first, last)         => s"Move along, $first"
    }
  }
}

object People extends App {
  println(Stormtrooper.inspect(Person("Noel", "Welsh")))
  println(Stormtrooper.inspect(Person("Han", "Solo")))
}
