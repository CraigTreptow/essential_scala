trait Feline {
  def color: String
  val sound = "meow"
}

trait BigCat extends Feline {
  override val sound = "roar"
}

case class Cat(color: String, food: String) extends Feline

case class Lion(color: String, maneSize: Int) extends BigCat

case class Tiger(color: String) extends BigCat

object ChipShop {
  def willServe(cat: Cat): Boolean = {
    cat match {
      case Cat(_, "chips") => true
      case Cat(_, _)       => false
    }
  }
}

object Cats extends App {
  val c = new Cat("blue", "Chips")
  val t = new Tiger("blue")
  val l = new Lion("blue", 9)

  println(ChipShop.willServe(c))

  println(c.sound)
  println(t.sound)
  println(l.sound)
  println(l.maneSize)
}
