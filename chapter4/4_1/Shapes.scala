trait Shape {
  def sides: Int
  def perimeter: Double
  def area: Double
}

trait Rectangular extends Shape {
  def width: Double
  def height: Double
  val sides = 4
  override val perimeter = 2 * width + 2 * height
  override val area = width * height
}

case class Circle(radius: Double) extends Shape {
  val sides = 1
  val perimeter = 2 * math.Pi * radius
  val area = math.Pi * radius * radius
}

case class Rectangle(width: Double, height: Double) extends Rectangular

case class Square(size: Int) extends Rectangular {
  val width = size
  val height = size
}

object Shapes extends App {
  val c = new Circle(3.0)
  val r = new Rectangle(1,2)
  val s = new Square(9)

  println(c.radius)
  println(r.perimeter)
  println(s.area)
}
