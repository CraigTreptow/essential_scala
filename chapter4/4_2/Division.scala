sealed trait DivisionResult
final case class Finite(value: Int) extends DivisionResult
case object Infinite extends DivisionResult

object divide {
  def apply(dividend: Int, divisor: Int): DivisionResult =
    if (divisor == 0) Infinite else Finite(dividend / divisor)
}

object Divider {
  def inspect(result: DivisionResult) = {
    result match {
      case Finite(value) => s"It's finite: ${value}"
      case Infinite      => s"It's infinite"
    }
  }
}

object Division extends App {
  println(s"1 divided by 1: ${Divider.inspect(divide(1, 1))}")
  println(s"1 divided by 0: ${Divider.inspect(divide(1, 0))}")
  println(s"0 divided by 1: ${Divider.inspect(divide(0, 1))}")
}

