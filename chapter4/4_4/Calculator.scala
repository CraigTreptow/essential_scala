// A calculation may succeed (with an Int result) or fail (with a String message).

// mine
sealed trait MyCalculation
final case class MySucceeded() extends MyCalculation
final case class MyFailed()    extends MyCalculation

// theirs
sealed trait TheirCalculation
final case class TheirSucceeded(result: Int) extends TheirCalculation
final case class TheirFailed(reason: String) extends TheirCalculation

