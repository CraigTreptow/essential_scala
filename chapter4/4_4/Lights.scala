// model: A traffic light is red, green, or yellow.

// mine
sealed trait Light
final case class Red()    extends Light
final case class Green()  extends Light
final case class Yellow() extends Light

// theirs
sealed trait TrafficLight
case object Red    extends TrafficLight
case object Green  extends TrafficLight
case object Yellow extends TrafficLight
