// Bottled water has a size (an Int), a source (which is a well, spring, or tap), and a Boolean carbonated.

// mine
sealed trait MySource
final case class Well()   extends MySource
final case class Spring() extends MySource
final case class Tap()    extends MySource
case class MyBottledWater(size: Int, source: Source, carbonated: Boolean)

// theirs
sealed trait Source
case object Well extends Source
case object Spring extends Source
case object Tap extends Source
final case class BottledWater(size: Int, source: Source, carbonated: Boolean)
