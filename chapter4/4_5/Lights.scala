// Implement method next using polymorphism, then pattern matching

// polymorphism
sealed trait TrafficLightPoly {
  def next: TrafficLightPoly
}
case object RedPoly extends TrafficLightPoly {
  def next: TrafficLightPoly =
    GreenPoly
}
case object GreenPoly extends TrafficLightPoly {
  def next: TrafficLightPoly =
    YellowPoly
}
case object YellowPoly extends TrafficLightPoly {
  def next: TrafficLightPoly =
    RedPoly
}

// Benefits of pattern matching version:
// next doesn't depend on external data
// This is the single implementation of the method
// This is clearer (all in one place)

sealed trait TrafficLight {
def next: TrafficLight =
	this match {
		case Red    => Green
		case Green  => Yellow
		case Yellow => Red
	}
}
case object Red    extends TrafficLight
case object Green  extends TrafficLight
case object Yellow extends TrafficLight

